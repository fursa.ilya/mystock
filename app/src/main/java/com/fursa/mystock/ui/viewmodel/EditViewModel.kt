package com.fursa.mystock.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.fursa.mystock.AppDelegate
import com.fursa.mystock.storage.entity.Product
import com.fursa.mystock.storage.repository.LocalRepository
import io.reactivex.Single
import javax.inject.Inject

class EditViewModel(application: Application) : AndroidViewModel(application) {

    init {
        AppDelegate.appComponent.inject(this)
    }

    @Inject
    lateinit var repository: LocalRepository

    fun update(id: Long, product: Product) {
        repository.update(id, product)
    }

    fun findById(id: Long): Single<Product> {
        return repository.findDetailsById(id)
    }
}