package com.fursa.mystock.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.fursa.mystock.AppDelegate
import com.fursa.mystock.storage.entity.Product
import com.fursa.mystock.storage.repository.LocalRepository
import javax.inject.Inject

class CreateViewModel(application: Application) : AndroidViewModel(application) {

    init {
        AppDelegate.appComponent.inject(this)
    }

    @Inject
    lateinit var repository: LocalRepository

    fun create(product: Product) {
        repository.insertProduct(product)
    }

}