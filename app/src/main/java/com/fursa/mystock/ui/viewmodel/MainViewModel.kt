package com.fursa.mystock.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.fursa.mystock.AppDelegate
import com.fursa.mystock.storage.entity.Product
import com.fursa.mystock.storage.repository.LocalRepository
import io.reactivex.Single
import javax.inject.Inject

class MainViewModel(application: Application) : AndroidViewModel(application) {

    init {
        AppDelegate.appComponent.inject(this)
    }

    @Inject
    lateinit var repository: LocalRepository

    fun selectAll(): Single<List<Product>> {
        return repository.selectAll()
    }

    fun deleteById(id: Long) {
        return repository.deleteById(id)
    }
}