package com.fursa.mystock.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fursa.mystock.R
import com.fursa.mystock.extenstions.toast
import com.fursa.mystock.storage.entity.Product

class ProductAdapter(private val listener: OnShowDetailsListener) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {
    private val mDataSource = mutableListOf<Product>()

    fun setDataSource(dataSource: MutableList<Product>) {
        mDataSource.clear()
        mDataSource.addAll(dataSource)
        notifyDataSetChanged()
    }

    var items = mDataSource

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val productView = LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false)
        return ProductViewHolder(productView)
    }

    override fun getItemCount() = mDataSource.count()

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val productItem = mDataSource[position]
        holder.bind(productItem)
    }

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), ItemTouchHelperCallback.ItemTouchViewHolder {
        private val txtName = itemView.findViewById<TextView>(R.id.txtName)
        private val txtPrice = itemView.findViewById<TextView>(R.id.txtPrice)


        fun bind(product: Product) {
            txtName.text = product.name
            txtPrice.text = "${product.price} $"

            itemView.setOnClickListener {
               listener.onProductItemSelected(product.id, product.picture.toString())
            }
        }

        override fun onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY)
        }

        override fun onItemCleared() {
            itemView.setBackgroundColor(Color.WHITE)
        }

    }

    interface OnShowDetailsListener {
        fun onProductItemSelected(productId: Long, productUri: String)
    }

}