package com.fursa.mystock.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.fursa.mystock.AppDelegate
import com.fursa.mystock.R
import com.fursa.mystock.extenstions.toast
import com.fursa.mystock.storage.entity.Product
import com.fursa.mystock.storage.repository.LocalRepository
import com.fursa.mystock.ui.common.BaseFullscreenFragment
import com.fursa.mystock.ui.viewmodel.CreateViewModel
import kotlinx.android.synthetic.main.fragment_create.*
import kotlinx.android.synthetic.main.fragment_create.view.*
import java.io.File
import javax.inject.Inject

const val GALLERY_PICK_CODE = 100
const val CONTENT_TYPE = "image/*"

class CreateFragment : BaseFullscreenFragment() {

    companion object {

        fun newInstance(): CreateFragment {
            return CreateFragment()
        }
    }

    override fun layoutId(): Int {
        return R.layout.fragment_create
    }

   // private var resultBitmap: Bitmap? = null
    private var resultUri: Uri? = null

    private var refreshListener: OnProductRefreshListener? = null

    private lateinit var createViewModel: CreateViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createViewModel = ViewModelProviders.of(this).get(CreateViewModel::class.java)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is OnProductRefreshListener) {
            refreshListener = context
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        buttonCreate.setOnClickListener {
            if(resultUri != null) {
                val title = textInputLayoutTitle.editTextTitle.text.toString()
                val price = textInputLayoutPrice.editTextPrice.text.toString().toDouble()
                createViewModel.create(Product(title, price, resultUri.toString()))
                refreshListener?.onProductAdded()
                dismiss()
            }
        }

        buttonAddImage.setOnClickListener { pickFromGallery() }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultIntent: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultIntent)
        if(requestCode == GALLERY_PICK_CODE && resultCode == Activity.RESULT_OK && resultIntent != null) {
            val imageUri = resultIntent.data
            if(imageUri != null) {
                val inputStream = context?.contentResolver?.openInputStream(imageUri)
                val bitmap = BitmapFactory.decodeStream(inputStream)
                productPicture.visibility = View.VISIBLE
                productPicture.setImageBitmap(bitmap)
                resultUri = resultIntent.data
                context?.toast(resultUri.toString())
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        refreshListener = null
    }

    private fun pickFromGallery() {
        val galleryIntent = Intent()
        galleryIntent.type = CONTENT_TYPE
        galleryIntent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(galleryIntent, "Выберите изображение"), GALLERY_PICK_CODE)
    }

    interface OnProductRefreshListener {
        fun onProductAdded()
    }
}