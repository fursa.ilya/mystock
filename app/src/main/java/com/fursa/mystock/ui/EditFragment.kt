package com.fursa.mystock.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.fursa.mystock.AppDelegate
import com.fursa.mystock.R
import com.fursa.mystock.extenstions.toast
import com.fursa.mystock.storage.entity.Product
import com.fursa.mystock.storage.repository.LocalRepository
import com.fursa.mystock.ui.common.BaseFullscreenFragment
import com.fursa.mystock.ui.viewmodel.EditViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_edit.*
import kotlinx.android.synthetic.main.fragment_edit.view.*
import javax.inject.Inject

const val PRODUCT_KEY = "id"
const val PRODUCT_URI = "uri"
const val GALLERY_PICK_NEW_CODE = 100
const val CONTENT_NEW_TYPE = "image/*"

class EditFragment : BaseFullscreenFragment() {
    private lateinit var editTextPrice: EditText
    private lateinit var editTextTitle: EditText
    private var resultUri: Uri? = null
    private var listener: OnProductRefreshListener? = null

    private lateinit var editViewModel: EditViewModel

    override fun layoutId(): Int {
        return R.layout.fragment_edit
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        editViewModel = ViewModelProviders.of(this).get(EditViewModel::class.java)
    }


    companion object {
        fun newInstance(productId: Long, oldUri: String): EditFragment {
            val fragment = EditFragment()
            val args = Bundle()
            args.apply {
                putLong(PRODUCT_KEY, productId)
                putString(PRODUCT_URI, oldUri)
            }
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is OnProductRefreshListener) {
            listener = context
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editTextTitle = textInputLayoutTitle.editTextTitle
        editTextPrice = textInputLayoutPrice.editTextPrice
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subs.add(
            editViewModel.findById(arguments?.getLong(PRODUCT_KEY)!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    editTextTitle.setText(it.name)
                    editTextPrice.setText(it.price.toString())
                    showImage(it.picture)
                }, {

                })
        )

        buttonUpdate.setOnClickListener {
            if(resultUri != null) {
                val title = textInputLayoutTitle.editTextTitle.text.toString()
                val price = textInputLayoutPrice.editTextPrice.text.toString().toDouble()
                val id = arguments!!.getLong(PRODUCT_KEY)
                editViewModel.update(id, Product(title, price, resultUri.toString()))
                listener?.onProductUpdated()
                dismiss()
            } else {
                val title = textInputLayoutTitle.editTextTitle.text.toString()
                val price = textInputLayoutPrice.editTextPrice.text.toString().toDouble()
                val id = arguments!!.getLong(PRODUCT_KEY)
                val oldUri = arguments!!.getString(PRODUCT_URI)
                editViewModel.update(id, Product(title, price, oldUri.toString()))
                listener?.onProductUpdated()
                dismiss()
            }
        }

        buttonAddImage.setOnClickListener {
            pickFromGallery()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultIntent: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultIntent)
        if(requestCode == GALLERY_PICK_NEW_CODE && resultCode == Activity.RESULT_OK && resultIntent != null) {
            val imageUri = resultIntent.data
            if(imageUri != null) {
                val inputStream = context?.contentResolver?.openInputStream(imageUri)
                val bitmap = BitmapFactory.decodeStream(inputStream)
                productPicture.visibility = View.VISIBLE
                productPicture.setImageBitmap(bitmap)
                resultUri = resultIntent.data
                productPicture.setImageURI(resultUri)
                context?.toast(resultUri.toString())
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun pickFromGallery() {
        val galleryIntent = Intent()
        galleryIntent.type = CONTENT_NEW_TYPE
        galleryIntent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(galleryIntent, "Выберите изображение"), GALLERY_PICK_NEW_CODE)
    }

    private fun showImage(path: String) {
       Glide.with(this)
           .load(Uri.parse(path))
           .into(productPicture)
    }

    interface OnProductRefreshListener {
        fun onProductUpdated()
    }
}