package com.fursa.mystock.ui

import android.app.Dialog
import android.os.Bundle
import com.fursa.mystock.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

open class RoundedDialog: BottomSheetDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog { return BottomSheetDialog(requireContext(), theme) }

    override fun getTheme() = R.style.BottomSheetDialogTheme
}

