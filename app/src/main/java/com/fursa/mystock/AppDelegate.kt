package com.fursa.mystock

import android.app.Application
import com.fursa.mystock.di.AppComponent
import com.fursa.mystock.di.DaggerAppComponent
import com.fursa.mystock.di.RoomModule


class AppDelegate : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .roomModule(RoomModule(this))
            .build()

        appComponent.inject(this)
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}