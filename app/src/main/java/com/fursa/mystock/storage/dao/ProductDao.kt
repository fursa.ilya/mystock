package com.fursa.mystock.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fursa.mystock.storage.entity.Product
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
abstract class ProductDao {

   @Insert(onConflict = OnConflictStrategy.REPLACE)
   abstract fun insert(product: Product)

   @Query("SELECT * FROM Product WHERE id=:productId")
   abstract fun findDetailsById(productId: Long): Single<Product>

   @Query("SELECT * FROM Product")
   abstract fun selectAll(): Single<List<Product>>

   @Query("DELETE FROM Product")
   abstract fun deleteAll()

   @Query("DELETE FROM Product WHERE id=:productId")
   abstract fun deleteById(productId: Long)

   @Query("UPDATE Product SET name=:title, price=:price, picture=:picture WHERE id=:productId")
   abstract fun update(productId: Long, title: String, price: Double, picture: String)

}