package com.fursa.mystock.storage.entity

import android.graphics.Bitmap
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Product")
data class Product(

    @ColumnInfo(name = "name")
    var name: String,

    @ColumnInfo(name = "price")
    var price: Double,

    /*@ColumnInfo(name = "picture", typeAffinity = ColumnInfo.BLOB)
    val picture: Bitmap*/
    @ColumnInfo(name = "picture")
    val picture: String
)
{

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0

    override fun toString(): String {
        return "Product(id=$id, name='$name', price=$price)"
    }

}