package com.fursa.mystock.storage

import android.content.Context
import androidx.room.*

import com.fursa.mystock.storage.dao.ProductDao
import com.fursa.mystock.storage.entity.Product

private const val databaseName = "ecwid.db"

@Database(
    entities = [Product::class],
    version = 2,
    exportSchema = false)
@TypeConverters(BitmapConverter::class)
abstract class LocalStorage: RoomDatabase() {
    abstract fun createProductDao(): ProductDao

    companion object {
        fun getInstance(context: Context): LocalStorage {
            return Room.databaseBuilder(context.applicationContext, LocalStorage::class.java, databaseName)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}