package com.fursa.mystock.storage

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.room.TypeConverter
import java.io.ByteArrayOutputStream

class BitmapConverter {

    @TypeConverter
    fun toByteArr(bitmapImage: Bitmap): ByteArray {
        val byteArrayStream = ByteArrayOutputStream()
        bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, byteArrayStream)
        return byteArrayStream.toByteArray()
    }

    @TypeConverter
    fun fromByteArr(byteArray: ByteArray): Bitmap {
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
    }
}