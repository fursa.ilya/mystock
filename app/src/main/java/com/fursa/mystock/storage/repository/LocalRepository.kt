package com.fursa.mystock.storage.repository

import android.os.AsyncTask
import com.fursa.mystock.storage.dao.ProductDao
import com.fursa.mystock.storage.entity.Product
import io.reactivex.Single

class LocalRepository(private val productDao: ProductDao) {

    fun insertProduct(product: Product) {
       InsertTask(productDao).execute(product)
    }

    fun findDetailsById(id: Long): Single<Product> {
        return productDao.findDetailsById(id)
    }

    fun selectAll(): Single<List<Product>> {
        return productDao.selectAll()
    }

    fun deleteAll() {
        productDao.deleteAll()
    }

    fun deleteById(id: Long) {
        productDao.deleteById(id)
    }

    fun update(id: Long, product: Product) {
       UpdateTask(productDao, id).execute(product)
    }

    inner class InsertTask(private val productDao: ProductDao): AsyncTask<Product, Void, Void>() {

        override fun doInBackground(vararg params: Product?): Void? {
            productDao.insert(params[0]!!)
            return null
        }

    }

    inner class UpdateTask(private val productDao: ProductDao, private val id: Long): AsyncTask<Product, Void, Void>() {

        override fun doInBackground(vararg params: Product?): Void? {
           val product = params[0]
           productDao.update(id, product?.name.toString(), product?.price.toString().toDouble(), product?.picture.toString())
           return null
        }
    }

}