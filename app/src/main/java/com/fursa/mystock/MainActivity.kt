package com.fursa.mystock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.fursa.mystock.extenstions.toast
import com.fursa.mystock.storage.repository.LocalRepository
import com.fursa.mystock.ui.CreateFragment
import com.fursa.mystock.ui.EditFragment
import com.fursa.mystock.ui.adapter.ItemTouchHelperCallback
import com.fursa.mystock.ui.adapter.ProductAdapter
import com.fursa.mystock.ui.viewmodel.MainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), ProductAdapter.OnShowDetailsListener, EditFragment.OnProductRefreshListener,
    SwipeRefreshLayout.OnRefreshListener,
    CreateFragment.OnProductRefreshListener {

    private val productAdapter = ProductAdapter(this)
    private val subs = CompositeDisposable()

    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        fab.setOnClickListener { openCreateFragment() }

        refresher.setOnRefreshListener(this)

        val callback = ItemTouchHelperCallback(productAdapter) { product ->
            mainViewModel.deleteById(product.id)
        }

        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(recyclerView)

        with(recyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = productAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        getProducts()
    }

    override fun onRefresh() {
        getProducts()
    }

    private fun openCreateFragment() {
        val createFragment = CreateFragment.newInstance()
        createFragment.show(supportFragmentManager, createFragment.tag)
    }

    private fun getProducts() {
        subs.add(
            mainViewModel.selectAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { refresher.isRefreshing = true }
                .doFinally { refresher.isRefreshing = false }
                .subscribe({
                    if(it.isEmpty()) {
                        refresher.isRefreshing = false
                        txtMessage.visibility = View.VISIBLE
                        recyclerView.visibility = View.GONE
                    } else {
                        refresher.isRefreshing = true
                        txtMessage.visibility = View.GONE
                        recyclerView.visibility = View.VISIBLE
                        productAdapter.setDataSource(it.toMutableList())
                    }
                    productAdapter.setDataSource(it.toMutableList())
                }, {
                    toast("${it.localizedMessage}")
                })
        )
    }

    override fun onProductItemSelected(productId: Long, oldUri: String) {
        val editFragment = EditFragment.newInstance(productId, oldUri)
        editFragment.show(supportFragmentManager, editFragment.tag)
    }

    override fun onProductAdded() {
       onRefresh()
    }

    override fun onProductUpdated() {
        onRefresh()
    }

    override fun onDestroy() {
        super.onDestroy()
        subs.clear()
    }
}
