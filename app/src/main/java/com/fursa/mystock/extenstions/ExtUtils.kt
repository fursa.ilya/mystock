package com.fursa.mystock.extenstions

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.fursa.mystock.ui.CreateFragment
import com.fursa.mystock.ui.EditFragment

fun Context.toast(message: String) { Toast.makeText(this, message, Toast.LENGTH_LONG).show() }
