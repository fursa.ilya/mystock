package com.fursa.mystock.di

import com.fursa.mystock.AppDelegate
import com.fursa.mystock.MainActivity
import com.fursa.mystock.ui.CreateFragment
import com.fursa.mystock.ui.EditFragment
import com.fursa.mystock.ui.viewmodel.CreateViewModel
import com.fursa.mystock.ui.viewmodel.EditViewModel
import com.fursa.mystock.ui.viewmodel.MainViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [RoomModule::class]
)
interface AppComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(appDelegate: AppDelegate)
    fun inject(createFragment: CreateFragment)
    fun inject(editFragment: EditFragment)
    fun inject(editViewModel: EditViewModel)
    fun inject(mainViewModel: MainViewModel)
    fun inject(createViewModel: CreateViewModel)
}