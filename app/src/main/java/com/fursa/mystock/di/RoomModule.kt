package com.fursa.mystock.di

import android.app.Application
import com.fursa.mystock.storage.LocalStorage
import com.fursa.mystock.storage.repository.LocalRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule(application: Application) {
    private val appContext = application.applicationContext

    private val db by lazy {
        LocalStorage.getInstance(appContext)
    }

    private val productDao by lazy {
        db.createProductDao()
    }

    @Singleton
    @Provides
    fun provideLocalStorage() = db

    @Singleton
    @Provides
    fun provideDao() = productDao

    @Singleton
    @Provides
    fun provideRepository() = LocalRepository(productDao)
}